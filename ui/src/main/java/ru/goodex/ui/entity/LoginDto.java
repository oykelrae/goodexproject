package ru.goodex.ui.entity;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class LoginDto {

    private String username;
    private String password;

}

