package ru.goodex.ui.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegistrationDto {

    private String email;
    private String username;
    private String password;
    private String firstName;
    private String secondName;

}
