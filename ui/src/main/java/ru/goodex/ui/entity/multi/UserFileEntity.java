package ru.goodex.ui.entity.multi;


import org.springframework.web.multipart.MultipartFile;
import ru.goodex.ui.entity.RegistrationDto;

public class UserFileEntity {
    private RegistrationDto userDto;
    private MultipartFile multipartFile;

    public RegistrationDto getUserDto() {
        return userDto;
    }

    public void setUserDto(RegistrationDto userDto) {
        this.userDto = userDto;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
