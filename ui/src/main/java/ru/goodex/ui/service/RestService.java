package ru.goodex.ui.service;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import ru.goodex.ui.entity.LoginDto;
import ru.goodex.ui.entity.RegistrationDto;
import ru.goodex.ui.entity.UserDTO;
import ru.goodex.ui.entity.multi.UserFileEntity;

@Service
public class RestService {
    private final RestTemplate template;
    private String webURL = "http://localhost:8080/web/";

    public RestService(RestTemplate template) {
        this.template = template;
    }

    public ResponseEntity register(RegistrationDto user, MultipartFile file) {
        UserFileEntity userFileEntity = new UserFileEntity();
        userFileEntity.setMultipartFile(file);
        userFileEntity.setUserDto(user);
        ResponseEntity result = template.postForEntity(webURL + "auth/registration",
                userFileEntity,
                Boolean.class);
        return result;
    }

    public ResponseEntity activate(String code) {
        ResponseEntity result = template.postForEntity(webURL + "auth/activation", code, Boolean.class);
        return result;
    }

    public ResponseEntity<UserDTO> login(LoginDto loginDto) {
        ResponseEntity<UserDTO> result = template.postForEntity(webURL + "auth/login", loginDto, UserDTO.class);
        return result;
    }
}
