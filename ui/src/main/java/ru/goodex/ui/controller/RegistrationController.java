package ru.goodex.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.goodex.ui.entity.RegistrationDto;
import ru.goodex.ui.service.RestService;


@Controller
public class RegistrationController {
    private RestService restService;

    @Autowired
    RegistrationController(RestService restService) {
        this.restService = restService;
    }

    @PostMapping("web/registrationAction")
    public String registrationAction(RegistrationDto user,
                                     @RequestParam(required = false) MultipartFile file,
                                     Model model) {
        ResponseEntity responseEntity = restService.register(user, file);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {

            return "activation";
        }
        model.addAttribute("UserAlreadyExist", "Такой пользователь уже существует");
        return "registration";
    }

    @PostMapping("web/activation")
    public String verify(@RequestParam String verifyCode, Model model) {
        ResponseEntity responseEntity = restService.activate(verifyCode);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return "home";
        }
        model.addAttribute("CodeError", "Неверный код. Попробуйте ещё раз");
        return "activation";
    }
}
