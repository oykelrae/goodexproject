package ru.goodex.ui.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {
    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/web/registration")
    public String registerPage() {
        return "registration";
    }

    @GetMapping("/web/loginpage")
    public String loginPage() {
        return "login";
    }
}
