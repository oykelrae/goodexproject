package ru.goodex.ui.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import ru.goodex.ui.entity.LoginDto;
import ru.goodex.ui.entity.UserDTO;
import ru.goodex.ui.service.RestService;


@Controller
public class LoginController {

    private RestService restService;

    @Autowired
    LoginController(RestService restService) {
        this.restService = restService;
    }

    @PostMapping("web/login")
    public String login(LoginDto loginDto, HttpServletResponse servletResponse, Model model) {
        ResponseEntity<UserDTO> response = restService.login(loginDto);
        if (response.getStatusCode().is2xxSuccessful()) {
            Cookie cookieToken = new Cookie("toke", response.getBody().getToken());
            cookieToken.setPath("/");
            cookieToken.setMaxAge(500000);
            servletResponse.addCookie(cookieToken);
            Cookie cookieId = new Cookie("id", response.getBody().getId().toString());
            cookieId.setPath("/");
            cookieId.setMaxAge(500000);
            servletResponse.addCookie(cookieId);
            servletResponse.setContentType("text/plain");
            return "home";
        }
        model.addAttribute("loginError", "Неверное имя пользователя или пароль");
        return "login";
    }
}
