package ru.goodex.web.entity.multi;


import org.springframework.web.multipart.MultipartFile;
import ru.goodex.web.entity.dto.RegistrationDTO;
import ru.goodex.web.entity.dto.UserDTO;

public class UserFileEntity {
    private RegistrationDTO userDto;
    private MultipartFile multipartFile;

    public RegistrationDTO getUserDto() {
        return userDto;
    }

    public void setUserDto(RegistrationDTO userDto) {
        this.userDto = userDto;
    }

    public MultipartFile getMultipartFile() {
        return multipartFile;
    }

    public void setMultipartFile(MultipartFile multipartFile) {
        this.multipartFile = multipartFile;
    }
}
